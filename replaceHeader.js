'use strict';

console.log('Loading function');

const aws = require('aws-sdk');
const async = require('async');
const util = require('util');
const columns = require("./config").columns;
const _ = require('lodash');
// aws.config.update({region: "us-west-2"});

const s3 = new aws.S3();

exports.handler = (event, context, callback) => {
    console.log("Reading options from event:\n", util.inspect(event, {depth: 5}));

    // Get the object from the event and show its content type
    const srcBucket = event.Records[0].s3.bucket.name;
    const srcKey = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
    const dstBucket = srcBucket.replace('-src', '');
    const lastIndex = srcKey.lastIndexOf('/')
    const dstFile = srcKey.substr(lastIndex + 1);
    let dstKey = '',
        pathToFile = '';

    const params = {
        Bucket: srcBucket,
        Key: srcKey
    };

    // Sanity Check
    if (srcBucket == dstBucket) {
        console.log("Source and destination buckets are identical");
        return;
    }

    async.waterfall([
            function download(next) {
                console.log("download start: " + srcKey);
                s3.getObject(params,
                    function(err, data) {
                        next(err, data);
                    }
                );
            },
            function replaceHeader(response, next) {
                let str = new String(response.Body);
                str.toString('utf-8');
                let column_list = {};
                switch (true) {
                    case /顧客/.test(srcKey):
                        column_list = columns.dic_customer_columns;
                        pathToFile = 'csv_dic_customer/'
                        break;
                    case /車両/.test(srcKey):
                        column_list = columns.dic_car_columns;
                        pathToFile = 'csv_dic_car/'
                        break;
                    case /点検/.test(srcKey):
                        column_list = columns.dic_inspection_columns;
                        pathToFile = 'csv_dic_inspection/'
                        break;
                    case /NX/.test(srcKey):
                        column_list = columns.nx_columns;
                        pathToFile = 'csv_nx/'
                        break;
                    case /POS/.test(srcKey):
                        column_list = columns.pos_columns;
                        pathToFile = 'csv_pos/'
                        break;
                    default:
                        console.log("Doesn't match with any filename pattern.")
                        return;
                }
                _.each(column_list, function(value, key, object) {
                    let regExp = new RegExp(value);
                    str = str.replace(regExp, key);
                });
                next(null, str);
            },
            function upload(data, next) {
                dstKey = pathToFile + dstFile;
                console.log("upload start: " + dstKey);
                s3.putObject({
                    Bucket: dstBucket,
                    Key: dstKey,
                    Body: data
                }, function(err, data) {
                    if (err) {
                        next(err);
                    }
                    try {
                        next(null);
                    } catch (e) {
                        next("Error occuring: " + e);
                        return;
                    }
                });
            }
        ], function (err) {
            if (err) {
                console.error("Error: " + err);
            } else {
                console.log("Success");
            }
        }
    );
    context.done(null, "Finish Lambda process");
};
