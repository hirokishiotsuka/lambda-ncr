//YOUR DATA TO BE PASSED TO LAMBDA FUNCTION.
const event = {
  "Records": [
    {
       "eventVersion": "2.0",
       "eventSource": "aws:s3",
      "awsRegion": "us-west-2",
      "eventTime": "2016-11-18T10:54:53.047Z",
      "eventName": "ObjectCreated:Put",
      "userIdentity": { "principalId": "AWS:AIDAIL2YSYW7JHOX7E5PE" },
      "requestParameters": { "sourceIPAddress": "111.101.74.8" },
      "responseElements": {
        "x-amz-request-id": "431D209D92B44428",
        "x-amz-id-2": "KRsRozG53LxRNmlzskbZa3iQu0+EQJPyGWIv/Cvb4TrHqYs3/nO5RlBrWtzTWyQY1B2LHgQBLDo="
       },
       "s3": {
          "s3SchemaVersion": "1.0",
          "configurationId": "dc920052-3e49-42a9-9241-d44907d87bd2",
          "bucket": {
            "name": "nikoniko-dev-src",
           "ownerIdentity": {
             "principalId": "A2RHTISRWPJSUZ"
           },
          "arn": "arn:aws:s3:::nikoniko-dev-src"
        },
        "object": {
          "key": "20046_祝田石油/001_JCS浜松本店/20046001_POS%E6%83%85%E5%A0%B1_20160921194230.csv",
          "size": 8311,
          "eTag": "eb7fd4aa1ac41c868e9780967ba6cd0f",
          "sequencer": "005831237105C83E86"
        }
      }
    }
  ]
};

//BUILD STAB OF context OBJECT.
const context = {
    invokeid: 'invokeid',
    done: function(err,message){
        return;
    }
};


//RUN YOUR HANDLER
const lambda = require("./replaceHeader");
lambda.handler(event,context);
