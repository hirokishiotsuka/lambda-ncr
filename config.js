exports.columns = {
   dic_customer_columns : {
      customer_id : "顧客管理番号",
      customer_optional_code : "顧客任意コード",
      kana : "フリガナ",
      name : "顧客氏名",
      honor_name : "顧客敬称",
      phone1 : "電話番号1",
      phone2 : "電話番号2",
      mobile : "携帯電話番号",
      fax : "FAX番号",
      email : "メールアドレス",
      zipcode : "郵便番号",
      address : "住所",
      address : "備考",
      customer_type : "個人・法人区分",
      birthdate : "生年月日",
      discount_rate : "工賃値引率"
   },
   dic_car_columns : {
      car_id : "車両管理番号",
      customer_id : "顧客管理番号",
      number_area : "車番_地域",
      number_type : "車番_車種",
      number_kana : "車番_かな",
      number_no : "車番_番号",
      model_no : "型式指定番号",
      type_no : "類別区分番号",
      model : "型式",
      registered_date : "登録年月日",
      first_registered_month : "初度登録年月",
      type_division : "種別区分",
      usage_division : "用途区分",
      usage_type : "自家用・事業用",
      body_shape : "車体形状",
      car_name : "車名",
      nickname : "通称名",
      capacity : "定員",
      loadage : "積載量",
      body_weight : "車両重量",
      total_weight : "車両総重量",
      manufacture_no : "車台番号",
      engine_type : "原動機型式",
      length : "長さ",
      width : "幅",
      height : "高さ",
      engine_displacement : "排気量",
      standard_output : "定規格出力",
      fuel_type : "燃料種",
      inspection_cycle : "車検サイクル",
      expire_date : "満了日",
      mileage : "走行距離",
      reception_date : "受付日",
      charge_person : "担当者",
      guide_type_inspection : "案内区分 車検",
      guide_type_maintenance : "案内区分 点検",
      car_number : "号車番号",
      car_inspection_date : "車検実施日",
      self_sale_division : "自社販売区分",
      vehicle_delivery_date : "納車日",
      insurance_expire_date : "任意保険満了日",
      insurance_company : "任意保険会社",
      insurance_division : "任意保険区分"
   },
   dic_inspection_columns : {
      car_id : "車両管理番号",
      inspecition_id : "点検番号",
      inspection_category : "整備カテゴリーコード",
      inspection_name : "整備名称",
      memo1 : "備考１",
      memo2 : "備考２",
      memo3 : "備考３",
      status_division : "状態区分",
      inspection_occur_date : "点検発生日",
      scheduled_date : "実施予定日",
      executed_date : "実施日",
   },
   nx_columns : {
      number_area : "車番_地域",
      number_type : "車番_車種",
      number_kana : "車番_かな",
      number_no : "車番_番号",
      visit_date : "来店日",
      call_target : "コール除外区分"
   },
   pos_columns : {
      line_no : "行NO",
      store_id : "店舗コード",
      store_name : "店舗名称",
      daily_date : "日計日付",
      data_no : "データNO",
      invoice_no : "請求データNO",
      process_division : "処理区分",
      process_division_name : "処理区分名称",
      deal_division : "取引区分",
      particular_division : "明細区分",
      particular_name : "明細名称",
      sub_deal_division : "サブ取引区分",
      client_code : "得意先コード",
      car_number : "車番",
      client_name : "得意先名称",
      member_code : "メンバーコード",
      deal_division_name : "取引区分名称",
      product_code : "商品コード",
      product_name : "商品名称",
      quantity : "数量",
      unit_amount : "単価",
      total_amount : "金額",
      tax_division : "税区分",
      tax_division_name : "税区分名",
      commercial_tax : "消費税",
      light_oil_tax : "軽油税",
      gross_profit : "粗利",
      point : "ポイント",
      car_serial_no : "カードシリアルNO",
      expiration_date : "有効期限",
      receipt_date : "伝票日付",
      particular_datetime : "明細時刻",
      delete_division : "削除区分",
      staff_code : "スタッフコード",
      staff_name : "スタッフ名称",
      error_code : "エラーコード",
      error_message : "エラーメッセージ",
      error_message_detail : "エラーメッセージ詳細",
      warning_code : "ワーニングコード",
      warning_message : "ワーニングメッセージ",
      warning_message_detail : "ワーニングメッセージ詳細",
      text_data : "テキストデータ",
      balance : "残高",
      selling_unit_price : "売価単価",
      selling_amount : "売価金額",
      monthly_aggregate_period : "月次集計年月",
      deputization_division : "代行区分",
      light_oil_tax_division : "軽油税区分",
      receipt_no : "伝票NO",
      lpg_flg : "LPGフラグ",
      memo : "備考",
      pump_no : "ポンプNO",
      accounts_receivable_remaining : "売掛残",
      prepaid_store_code : "プリカ店舗コード",
      client_store_code : "得意先店舗コード",
      car_division : "カード区分",
      client : "得意先",
      cd : "CD",
      cashback_discount_unit_price : "キャッシュバック値引単価",
      cashback_discount_total_amount : "キャッシュバック値引金額",
      discount_unit_price : "値引単価",
      discount_total_amount : "値引金額",
      discount_division : "値引区分",
      credit_over : "与信オーバー"
   }
};